class spriteSheetAnim {
    private   global: {} = window;
    private  currState: string = "s_pause";
    private   frameWidth: number;
    private   frameHeight: number;
    private   sheetHeight: number;
    private   sheetWidth: number;
    private   totalFrames: number;
    private  imgUrl:string;

    private   speed: number;
    private   allowLoop: boolean = false;

    // DO NOT EDIT VARS
    private  currFrame: number=1;
    private  spritesInX:number;
    private  spritesInY:number;

    private  animDiv:any;
    private  rangeStart:number = 0;
    private  rangeEnd:number = 1;

    private  mainLoop:any = null;

   
    constructor(animDiv: string, imgUrl: string, frameWidth: number, frameHeight: number, sheetWidth: number, sheetHeight: number, totalFrames: number, allowLoop:boolean,speed:number) {
        
        this.animDiv = document.getElementById(animDiv);
        this.imgUrl = imgUrl;
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;
        this.sheetWidth = sheetWidth;
        this.sheetHeight = sheetHeight;
        this.totalFrames = totalFrames;
        this.allowLoop = allowLoop;
        this.speed = speed;
        this.spritesInX = this.sheetWidth / this.frameWidth;
        this.spritesInY = this.sheetHeight / this.frameHeight;
        this.setUpSheet();

    }
    
    setUpSheet = () => {
        this.animDiv.style.position = "absolute";
        this.animDiv.style.width = this.frameWidth + "px";
        this.animDiv.style.height = this.frameHeight + "px";
        this.animDiv.style.backgroundImage = "url(" + this.imgUrl + ")";
        this.animDiv.style.backgroundRepeat = "no-repeat";
        this.animDiv.style.backgroundPosition = "0px 0px";
        
        
    }
    

     requestFrame =  () => {
        if (this.currState == "s_play") {

            if (this.currFrame > this.totalFrames) {
                this.currFrame = 1;
            } else if (this.currFrame == this.totalFrames && this.allowLoop == false) {
                this.currState = "s_pause";
                this.currFrame = this.totalFrames;
            }

            var xpos = ((this.currFrame - 1) % this.spritesInX);
            var ypos = Math.floor(((this.currFrame - 1) / this.spritesInX));

            this.animDiv.style.backgroundPosition = (-xpos * this.frameWidth) + "px " + (-ypos * this.frameHeight) + "px";
            this.currFrame++;

        } else if (this.currState == "s_rewind") {
            if (this.currFrame < 1) {
                this.currFrame = this.totalFrames;
            } else if (this.currFrame < 1 && this.allowLoop == false) {
                this.currFrame = 1;
                this.currState = "s_pause";
            }

            if (this.currFrame < 1) {
                this.currFrame = this.totalFrames;
            }

            this.upDateSheet();
            this.currFrame--;

        } else if (this.currState == "play_range") {
            if (this.rangeStart < this.rangeEnd) {

                // Playing forward
                if (this.currFrame < this.rangeEnd) {
                    this.upDateSheet();
                    this.currFrame++;
                } else {
                    this.currState = "s_pause";
                }
            } else {

                // Playing backward
                if (this.currFrame > this.rangeEnd) {
                    this.upDateSheet();
                    this.currFrame--;
                } else {
                    this.currState = "s_pause";
                }
            }

        } else if (this.currState == "s_pause") {
           this.ss_deActivate();
        }
}

	 upDateSheet =  () => {
    var xpos = ((this.currFrame - 1) % this.spritesInX);
    var ypos = Math.floor(((this.currFrame - 1) / this.spritesInX));
    this.animDiv.style.backgroundPosition = (-xpos * this.frameWidth) + "px " + (-ypos * this.frameHeight) + "px";
}
ss_activate =  () => {
    this.ss_deActivate();
    this.mainLoop = setInterval(this.requestFrame, this.speed);
}
ss_deActivate = function () {
    clearInterval(this.mainLoop);
}

ss_play = function () {
    this.ss_activate();
    this.currState = "s_play";
}

ss_rewind =  () => {
    this.ss_activate();
    this.currState = "s_rewind";
}


ss_pause = function () {
    this.currState = "s_pause";
}

ss_goToFrame = function (frame) {
    this.ss_deActivate();
    this.currFrame = frame;
    this.upDateSheet();
}


 ss_playRange = function (start, end) {
    this.ss_activate();
    this.rangeStart = start;
    this.rangeEnd = end;
    this.currFrame = this.rangeStart;
    this.currState = "play_range";
}




}